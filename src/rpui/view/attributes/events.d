/**
 * Events listeners attributes.
 *
 * Copyright: © 2017 RedGoosePaws
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Andrey Kabylin
 */

module rpui.view.attributes.events;

struct OnClickListener { string widgetName; }
struct OnDblClickListener { string widgetName; }
struct OnFocusListener { string widgetName; }
struct OnBlurListener { string widgetName; }
struct OnKeyPressedListener { string widgetName; }
struct OnKeyReleasedListener { string widgetName; }
struct OnTextEnteredListener { string widgetName; }
struct OnMouseMoveListener { string widgetName; }
struct OnMouseWheelListener { string widgetName; }
struct OnMouseEnterListener { string widgetName; }
struct OnMouseLeaveListener { string widgetName; }
struct OnMouseDownListener { string widgetName; }
struct OnMouseUpListener { string widgetName; }
